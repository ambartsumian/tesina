package ar.edu.info.unlp.constants;

import ar.edu.info.unlp.tesina.lectura.grafo.algoritmos.masivos.BusquedaDeCaminosNavegacionalesWikiquotesMasivo;

public class ConstantesDeProcesamientoDeTexto {

	public static final String SEPARADOR_VALORES_EN_LINEA = "\t";

	public static final String SEPARADOR_LINEAS = "\n";

	public static final String SEPARADOR_PARES_VERTICES = "-@@@-";

	/**
	 * @Deprecated Usar el tercer parametro de
	 *             {@link BusquedaDeCaminosNavegacionalesWikiquotesMasivo}
	 */
	@Deprecated
	public static final String SEPARADOR_VERTICES = "-@-";

}