package ar.edu.info.unlp.tesina.generacion.caminos.navegacionales.grafo;

import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote.MAX_SUPERSTEPS;
import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote.SOURCE_ID;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.giraph.GiraphRunner;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.job.GiraphJob;
import org.apache.giraph.utils.FileUtils;
import org.apache.giraph.utils.InternalVertexRunner;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote;
import ar.edu.info.unlp.tesina.vertice.estructuras.IdTextWithComplexValueInputFormat;
import ar.edu.info.unlp.tesina.vertice.estructuras.IdTextWithComplexValueOutputFormat;

/**
 * Permite generar los caminos navegacionales para un grafo dado, indicando a
 * traves de parametros la profundidad maxima de los caminos navegacionales y el
 * vertice que se debe tomar como origen. Se usan los primeros dos parametros,
 * el resto son redireccionados para que Giraph haga uso de ellos
 * 
 * <ul>
 * <li>Profundidad maxima de camino navegacional: args[0]</li>
 * <li>Vertice de origen: args[1]</li>
 * </ul>
 * 
 * 
 * @author jlarroque
 * @deprecated Usar {@link GeneradorCaminosNavegacionalesPorParesDeVertices}
 *
 */
@Deprecated
public class GeneradorCaminosNavegacionalesWikiquote implements Tool {

	private static final String[] EMPTY_STRING_ARRAY = new String[0];

	public static void main(String[] args) throws Exception {
		new GeneradorCaminosNavegacionalesWikiquote().run(args);
	}

	/**
	 * Helper method to remove an old output directory if it exists, and set the
	 * output path for any VertexOutputFormat that uses FileOutputFormat.
	 *
	 * @param job
	 *            Job to set the output path for
	 * @param outputPath
	 *            Path to output
	 * @throws IOException
	 */
	public static void removeAndSetOutput(GiraphJob job, Path outputPath)
			throws IOException {
		FileUtils.deletePath(job.getConfiguration(), outputPath);
		FileOutputFormat.setOutputPath(job.getInternalJob(), outputPath);
	}

	/**
	 * Prepare a GiraphJob
	 *
	 * @param name
	 *            identifying name for job
	 * @param conf
	 *            GiraphConfiguration describing which classes to use
	 * @param outputPath
	 *            Where to right output to
	 * @return GiraphJob configured for testing
	 * @throws IOException
	 *             if anything goes wrong
	 */
	protected static GiraphJob prepareJob(String name, Path outputPath,
			GiraphConfiguration configuracion) throws IOException {
		GiraphJob job = new GiraphJob(configuracion, name);
		if (outputPath != null) {
			removeAndSetOutput(job, outputPath);
		}
		return job;
	}

	public static String[] devolverGrafoInputModoTest() {
		return new String[] {
				"Portada	0.0	Sugerencias	1.0",
				"Proverbios	0.0",
				"Neil	0.0	Luna	1.0	ideal	1.0	verdad	1.0	Categoria:Ingenieros	2.0	Categoria:Estadounidenses	2.0	Categoria:Astronautas	2.0",
				"Categoria:Ingenieros	1.0	Neil	2.0",
				"Categoria:Estadounidenses	1.0	Neil	2.0",
				"Categoria:Astronautas	1.0	Neil	2.0", "Luna	1.0", "ideal	2.0",
				"Sugerencias	3.0	Neil	0.0" };
	}

	public void setConf(Configuration conf) {
	}

	public Configuration getConf() {
		return null;
	}

	public int run(String[] args) throws Exception {

		// Cambiar flag para modo TEST
		Boolean modoTest = Boolean.FALSE;

		String parametroLongitudCaminosNavegacionales = args[0];
		String parametroVerticeOrigen = args[1];

		GiraphConfiguration giraphConfiguration = new GiraphConfiguration();

		GiraphRunner giraphRunner = new GiraphRunner();

		System.out.println("----------------------------------------");
		System.out.println("Iniciando BFS con ID:" + parametroVerticeOrigen);
		System.out.println("----------------------------------------");

		args = argumentosGiraph(args);

		args[8] = args[8] + "/" + parametroVerticeOrigen;

		SOURCE_ID.set(giraphConfiguration, parametroVerticeOrigen);
		MAX_SUPERSTEPS.set(giraphConfiguration,
				Integer.valueOf(parametroLongitudCaminosNavegacionales));

		// Borro si existe el directorio, de otra forma explota
		FileUtils.deletePath(giraphConfiguration, args[8]);

		giraphRunner.setConf(giraphConfiguration);

		if (modoTest) {// Ejecucion desde dentro del eclipse
			giraphConfiguration
					.setComputationClass(BusquedaDeCaminosNavegacionalesWikiquote.class);
			giraphConfiguration
					.setVertexInputFormatClass(IdTextWithComplexValueInputFormat.class);
			giraphConfiguration
					.setVertexOutputFormatClass(IdTextWithComplexValueOutputFormat.class);
			InternalVertexRunner.run(giraphConfiguration,
					devolverGrafoInputModoTest());

		} else {// Modo comun, ejecucion desde linea de comandos

			int result = giraphRunner.run(args);
			System.out.println("----------------------------------------");
			System.out.println("El resultado del BFS con ID:"
					+ parametroVerticeOrigen + " result es:" + result);
			System.out.println("----------------------------------------");
		}

		return 0;
	}

	public String[] argumentosGiraph(String[] args_originales) {

		List<String> list = new ArrayList<String>();
		String[] array = list.toArray(new String[list.size()]);
		Collections.addAll(list, args_originales);
		list.removeAll(Arrays.asList(args_originales[0], args_originales[1]));
		array = list.toArray(EMPTY_STRING_ARRAY);
		return array;
	}

}
