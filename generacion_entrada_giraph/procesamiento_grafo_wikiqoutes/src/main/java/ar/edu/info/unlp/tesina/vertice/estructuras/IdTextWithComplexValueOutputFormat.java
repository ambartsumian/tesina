package ar.edu.info.unlp.tesina.vertice.estructuras;

import java.io.IOException;

import org.apache.giraph.graph.Vertex;
import org.apache.giraph.io.formats.IdWithValueTextOutputFormat;
import org.apache.giraph.io.formats.TextVertexOutputFormat;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.log4j.Logger;

import ar.edu.info.unlp.constants.ConstantesDeProcesamientoDeTexto;

/**
 * @author jlarroque
 *
 */
public class IdTextWithComplexValueOutputFormat
		extends
		TextVertexOutputFormat<Text, TextAndDoubleComplexWritable, DoubleWritable> {

	/** Class logger */
	private static final Logger LOG = Logger
			.getLogger(IdTextWithComplexValueOutputFormat.class);

	/** Specify the output delimiter */
	public static final String LINE_TOKENIZE_VALUE = "output.delimiter";
	/** Default output delimiter */
	public static final String LINE_TOKENIZE_VALUE_DEFAULT = ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA;
	/** Reverse id and value order? */
	public static final String REVERSE_ID_AND_VALUE = "reverse.id.and.value";
	/** Default is to not reverse id and value order. */
	public static final boolean REVERSE_ID_AND_VALUE_DEFAULT = false;

	@Override
	public TextVertexOutputFormat<Text, TextAndDoubleComplexWritable, DoubleWritable>.TextVertexWriter createVertexWriter(
			TaskAttemptContext context) throws IOException,
			InterruptedException {

		return new IdWithValueVertexWriter();
	}

	/**
	 * Vertex writer used with {@link IdWithValueTextOutputFormat}.
	 */
	protected class IdWithValueVertexWriter extends TextVertexWriterToEachLine {
		/** Saved delimiter */
		private String delimiter;
		/** Cached reserve option */
		private boolean reverseOutput;

		@Override
		public void initialize(TaskAttemptContext context) throws IOException,
				InterruptedException {
			super.initialize(context);
			delimiter = getConf().get(LINE_TOKENIZE_VALUE,
					LINE_TOKENIZE_VALUE_DEFAULT);
			reverseOutput = getConf().getBoolean(REVERSE_ID_AND_VALUE,
					REVERSE_ID_AND_VALUE_DEFAULT);
		}

		protected Text convertVertexToLine(
				Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex)
				throws IOException {
			StringBuilder str = new StringBuilder();

			if (!reverseOutput) {
				imprimirCaminosNavegacionales(vertex, str);
			}

			return new Text(str.toString());
		}

		/**
		 * Imprime caminos navegacionales que tienen al vertice como DESTINO
		 * 
		 * @param vertex
		 * @param str
		 */
		private void imprimirCaminosNavegacionales(
				Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex,
				StringBuilder str) {
			String mensajesVerticesAnteriores = vertex.getValue()
					.getMensajesVerticesPredecesores();

			// Agregamos al principio de la linea el vertice de destino de los
			// caminos navegacionales
			if (vertex.getValue().esVerticeDestino()) {
				LOG.info("Imprimiendo caminos navegacionales resultantes - Vertice destino : "
						+ vertex.getId().toString());
				str.append(vertex.getValue().getIdVerticeInicio());
				str.append(ConstantesDeProcesamientoDeTexto.SEPARADOR_PARES_VERTICES);
				str.append(vertex.getId().toString());
				str.append(ConstantesDeProcesamientoDeTexto.SEPARADOR_PARES_VERTICES);

				// Si el vertice tiene caminos navegacionales para imprimir, se
				// los
				// imprime
				if (!mensajesVerticesAnteriores.isEmpty()) {
					String separador = ConstantesDeProcesamientoDeTexto.SEPARADOR_LINEAS;
					for (String mensajeVerticeAnterior : mensajesVerticesAnteriores
							.split(ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA)) {

						str.append(separador);

						// LOG.debug(vertex.getId().toString()
						// + "- Camino encontrado"
						// + mensajeVerticeAnterior);
						str.append(mensajeVerticeAnterior);
					}
				} else {
					// LOG.debug(vertex.getId().toString() +
					// "- Sin resultados");
				}
			}
		}
	}
}
