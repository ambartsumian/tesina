package ar.edu.info.unlp.tesina.lectura.grafo;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.giraph.conf.IntConfOption;
import org.apache.giraph.conf.StrConfOption;
import org.apache.giraph.examples.Algorithm;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import ar.edu.info.unlp.constants.ConstantesDeProcesamientoDeTexto;
import ar.edu.info.unlp.tesina.lectura.archivo.ParserUtils;
import ar.edu.info.unlp.tesina.vertice.estructuras.TextAndDoubleComplexWritable;

/**
 * Busca todos los caminos navegacionales. Adaptacion de
 * https://github.com/MarcoLotz
 * /GiraphBFSSO/blob/master/src/uk/co/qmul/giraph/structurebfs
 * /SimpleBFSStructureComputation.java de @MarcoLotz, a los datos de
 * wikiquotes/wikipedia generados por el proyecto generacion_de_grafo_wikiquotes
 */
@Algorithm(name = "Busqueda de Caminos Navegacionales", description = "Busca todos los caminos navegacionales de un grafo, preparado para grafos de datos de Wikiquotes")
public class BusquedaDeCaminosNavegacionalesWikiquote
		extends
		BasicComputation<Text, TextAndDoubleComplexWritable, DoubleWritable, Text> {

	/** Class logger */
	private static final Logger LOG = Logger
			.getLogger(BusquedaDeCaminosNavegacionalesWikiquote.class);

	/**
	 * Id origen de la busqueda de caminos navegacionales, indica los vertices
	 * de inicio, a procesar en el superstep 0
	 */
	public static final StrConfOption SOURCE_ID = new StrConfOption(
			"BusquedaDeCaminosNavegacionales.sourceId", "Portada",
			"El vertice de origen, de la busqueda de todos los caminos navegacionales");

	/**
	 * Id origen de la busqueda de caminos navegacionales, indica los vertices
	 * de inicio, a procesar en el superstep 0
	 */
	public static final StrConfOption DESTINATION_ID = new StrConfOption(
			"BusquedaDeCaminosNavegacionales.destId", "Portada",
			"Vertices de destino, de la busqueda de todos los caminos navegacionales");

	/**
	 * Pares de vertice inicio-destino
	 */
	public static final StrConfOption PARES_INICIO_DESTINO = new StrConfOption(
			"BusquedaDeCaminosNavegacionales.pares", "",
			"Los pares de vertices inicio-destino");

	/**
	 * Define a maximum number of supersteps
	 */
	public static final IntConfOption MAX_SUPERSTEPS = new IntConfOption(
			"BusquedaDeCaminosNavegacionales.supersteps", 5,
			"Cantidad maxima de superpasos");

	public static final StrConfOption SEPARADOR_VERTICES = new StrConfOption(
			"BusquedaDeCaminosNavegacionales.separador", "",
			"Separador de vertices definido por el usuario");

	/**
	 * Separador de los valores en una misma linea
	 */
	public static final String LINE_TOKENIZE_VALUE_DEFAULT = "\t";

	public String getSeparadorDeVertices() {
		return SEPARADOR_VERTICES.get(getConf()).toString();
	}

	/**
	 * Se controla si el vertice corresponde a uno de los <b>multiples vertices
	 * de inicio</b>
	 *
	 * @param vertex
	 *            Vertex
	 * @return True if the source id
	 */
	public boolean esInicio(Vertex<Text, ?, ?> vertex) {
		String[] coleccionDeVertices = SOURCE_ID.get(getConf()).toString()
				.split(getSeparadorDeVertices());
		return contieneVertice(coleccionDeVertices, vertex.getId().toString());
	}

	public boolean contieneVertice(String[] coleccionDeVertices,
			String verticeBuscado) {
		return Arrays.asList(coleccionDeVertices).contains(verticeBuscado);
	}

	private int maximaCantidadDeSuperpasosPosibles() {
		return MAX_SUPERSTEPS.get(getConf());
	}

	/**
	 * Enviamos mensajes a todos los vecinos. El contenido del mensaje servira
	 * tanto para despertar a los vecinos, como para enviarles los distintos
	 * predecesores que tiene ese vertice en un camino navegacional dado.
	 * Ejemplo: Si el vertice B tiene como predecesor a J, al enviar a sus
	 * vecinos Z y X mensajes, le enviara lo siguiente a ambos: J B (es decir,
	 * los vertices que lo precedieron, y al final el mismo)
	 * 
	 * @param vertex
	 * @param mensajesPorEnviar
	 */
	public void BFSMessages(
			Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex,
			List<String> mensajesPorEnviar) {

		Map<String, ArrayList<String>> caminosPorVerticeDeInicio = caminosNavegacionalesParaReenviarAVerticesVecinos(vertex);

		// List<String> resultadosAReenviar = new ArrayList<String>();
		List<String> resultadosAImprimirComoSalida = new ArrayList<String>();

		// Este for debería reveerse en caso de tener multiples vertices de
		// inicio
		for (String verticeInicio : caminosPorVerticeDeInicio.keySet()) {
			// Si el vertice que estamos recorriendo es un vertice de destino
			if (esDestino(vertex)) {
				resultadosAImprimirComoSalida.addAll(caminosPorVerticeDeInicio
						.get(verticeInicio));
			}
			// else {
			// resultadosAReenviar.addAll(caminosPorVerticeDeInicio
			// .get(verticeInicio));
			// resultadosAReenviar.addAll(mensajesPorEnviar);
			// }
		}

		// En caso de que sea un vertice destino, debemos guardar los mensajes
		// resultantes (sin perder resultados anteriores)
		vertex.getValue().setMensajesVerticesPredecesores(
				armarMensajesSeparadosPorTAB(resultadosAImprimirComoSalida));

		// Como puede haber pares de vertices cuyo destino no es el vertice
		// actual, reenviamos los mensajes que se corresponden con vertices de
		// destino que no han sido aún encontrados, solo en el caso que que no
		// estemos en el ultimo superpaso (si lo estamos, nadie recibira los
		// mensajes en el superpaso siguiente, dado que se detendran los
		// vertices sin importar nada mas)
		if (getSuperstep() < maximaCantidadDeSuperpasosPosibles()) {

			// Usamos el combiner para aglutinar varios mensajes en uno
			// mensajesPorEnviar =
			// comprimirCantidadDeMensajesAEnviar(mensajesPorEnviar);

			for (String mensajeDeUnVerticePredecesor : mensajesPorEnviar) {
				// LOG.debug("Vertice: " + vertex.getId().toString()
				// + " enviando: " + mensajeDeUnVerticePredecesor);
				sendMessageToAllEdges(vertex, new Text(
						mensajeDeUnVerticePredecesor));
			}
		}

	}

	private Map<String, ArrayList<String>> caminosNavegacionalesParaReenviarAVerticesVecinos(
			Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex) {

		Map<String, ArrayList<String>> hashmap = new HashMap<String, ArrayList<String>>();
		String verticeDeInicio;

		// Separamos mensajes por su vertice de inicio
		for (String mensaje : vertex
				.getValue()
				.getMensajesVerticesPredecesores()
				.split(ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA)) {
			verticeDeInicio = getVerticeDeInicio(mensaje);
			if (!hashmap.containsKey(verticeDeInicio)) {
				hashmap.put(verticeDeInicio, new ArrayList<String>());
			}
			hashmap.get(verticeDeInicio).add(mensaje);

		}
		return hashmap;
	}

	private String getVerticeDeInicio(String mensaje) {

		return mensaje.split(getSeparadorDeVertices())[0];
	}

	@Override
	public void compute(
			Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex,
			Iterable<Text> messages) throws IOException {

		// LOG.debug("Vertex ID: " + vertex.getId() + " Superstep ID: "
		// + getSuperstep());

		// Se fuerza a que los vertices converjan en el superpaso máximo
		if (getSuperstep() <= maximaCantidadDeSuperpasosPosibles()) {

			List<String> mensajesPorEnviar = new ArrayList<String>();

			// Solo el vertice de inicio y los de destino deberia trabajar en el
			// primer superpaso
			// Todos los demas deberian detenerse y esperar por mensajes
			if (getSuperstep() == 0) {
				if (esInicio(vertex)) { // Es vertice de inicio
					LOG.info("Vertice inicial: " + vertex.getId().toString()
							+ " encontrado");
					vertex.getValue().setMensajesVerticesPredecesores(
							vertex.getId().toString());
					mensajesPorEnviar.add(vertex.getId().toString());
					BFSMessages(vertex, mensajesPorEnviar);

					// LOG.debug("[Vertice inicial] Vertex ID: " +
					// vertex.getId()
					// + " inicializado" + " Superstep ID: "
					// + getSuperstep());

				} else {

					if (esDestino(vertex)) { // Es vertice de destino
						vertex.getValue().setVerticeDestino(Boolean.TRUE);
						vertex.getValue()
								.setIdVerticeInicio(getVerticeInicio());
						LOG.info("Vertice destino: "
								+ vertex.getId().toString() + " encontrado");
					}

					// LOG.debug("Vertex ID: " + vertex.getId()
					// + " Superstep ID: " + getSuperstep()
					// + "inicializado");
				}
			} else {

				if ((getSuperstep() < maximaCantidadDeSuperpasosPosibles() || (getSuperstep() == maximaCantidadDeSuperpasosPosibles() && esDestino(vertex)))) {

					// LOG.debug("Vertex ID: " + vertex.getId()
					// + " Superstep ID: " + getSuperstep()
					// + " inicia descompresion mensajes.");

					// Usamos el combiner para aglutinar varios mensajes en uno
					Iterable<Text> mensajesDescomprimidos = descomprimirCantidadDeMensajesRecibidos(messages);

					// LOG.debug("Vertex ID: " + vertex.getId()
					// + " Superstep ID: " + getSuperstep()
					// + " inicia procesamiento mensajes.");

					// Procesamos los mensajes que llegan desde los vertices
					// predecesores
					mensajesPorEnviar.addAll(configurarMensajesDePredecesores(
							vertex, mensajesDescomprimidos));

					// LOG.debug("Vertex ID: " + vertex.getId()
					// + " Superstep ID: " + getSuperstep()
					// + " finalizo proc. mensajes predecesores");

					// Continuar con el resto de la estructura del grafo
					BFSMessages(vertex, mensajesPorEnviar);

					// LOG.debug("Vertex ID: " + vertex.getId()
					// + " Superstep ID: " + getSuperstep()
					// + " finaliza envio mensajes");
				} else {
					// LOG.debug("Vertex ID: " + vertex.getId()
					// + " Superstep ID: " + getSuperstep()
					// + " - No es destino - No se hace procesamiento");

					// Vaciamos los mensajes de predecesores, de otro modo seran
					// listados como caminos navegacionales en el output
					vertex.getValue().setMensajesVerticesPredecesores("");

				}
			}
		}
		// LOG.debug("Vertice ID " + vertex.getId() + " -> Vote to halt");
		vertex.voteToHalt();
	}

	/**
	 * Descomprime mensajes que habian sido comprimidos previamente. Si el
	 * mensaje no está comprimido, no hace nada, devuelve el mismo.
	 * 
	 * @param messages
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Iterable<Text> descomprimirCantidadDeMensajesRecibidos(
			Iterable<Text> messages) {
		List<Text> mensajesDescomprimidos = new ArrayList<Text>();
		String[] mensajesSueltos;
		for (Text mensajeADescomprimir : messages) {

			mensajesSueltos = mensajeADescomprimir.toString().split(
					ConstantesDeProcesamientoDeTexto.SEPARADOR_PARES_VERTICES);

			mensajesDescomprimidos.addAll(CollectionUtils.collect(
					Arrays.asList(mensajesSueltos), new Transformer() {

						public Object transform(Object input) {
							return new Text((String) input);
						}
					}));
		}
		return mensajesDescomprimidos;
	}

	private String armarMensajesSeparadosPorTAB(List<String> mensajes) {
		// String mensajesArmados = "";
		String separador = "";
		StringBuilder mensajesArmadosStringBuilder = new StringBuilder();
		for (String mens : mensajes) {
			// mensajesArmados += separador + mens;
			mensajesArmadosStringBuilder.append(separador).append(mens);
			separador = ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA;
		}
		return mensajesArmadosStringBuilder.toString();
	}

	private String getVerticeInicio() {
		// Este metodo debería adapatarse en caso de que haya varios vertices de
		// inicio
		return SOURCE_ID.get(getConf()).toString()
				.split(getSeparadorDeVertices())[0];
	}

	public boolean esDestino(
			Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex) {
		String[] coleccionDeVertices = DESTINATION_ID.get(getConf()).toString()
				.split(getSeparadorDeVertices());
		return contieneVertice(coleccionDeVertices, vertex.getId().toString());
	}

	/**
	 * Este metodo configura los vertices predecesores. En caso de que la
	 * cantidad maxima de superpasos haya sido alcanzada, permite recolectar
	 * mansajes enviados en el ultimo superpaso valido para procesamiento
	 * 
	 * @param vertex
	 * @param messages
	 */
	private List<String> configurarMensajesDePredecesores(
			Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex,
			Iterable<Text> messages) {

		Iterator<Text> iteradorDeMensajes = messages.iterator();

		List<String> mensajesAEnviar = new ArrayList<String>();

		// Se configuran los predecesores al vertice en caso de que
		// haya
		if (iteradorDeMensajes.hasNext()) {

			// LOG.debug("Vertex ID: " + vertex.getId()
			// + "Hay mensajes predecesores");

			// Obtenemos los predecesores hasta el momento (puede haber
			// de un procesamiento previo)
			String mensajesDeVerticesPredecesores = vertex.getValue()
					.getMensajesVerticesPredecesores();

			// Dado que pueden llegar a hacerse muchas concatenaciones, es
			// necesario usar un StringBuilder
			StringBuilder mensajesDeVerticesPredecesoresStringBuilder = new StringBuilder(
					mensajesDeVerticesPredecesores);

			String separador;
			if (mensajesDeVerticesPredecesores == null
					|| (mensajesDeVerticesPredecesores != null && mensajesDeVerticesPredecesores
							.isEmpty())) {
				separador = "";
			} else {
				separador = ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA;
			}

			String mensajeAEnviar;
			while (iteradorDeMensajes.hasNext()) {

				Text next = iteradorDeMensajes.next();
				// LOG.debug("Vertex ID: " + vertex.getId()
				// + "- Mensaje predecesor:" + next.toString());
				// Si el mensaje viene de un vertice predecesor (es decir, no
				// hicimos un camino ciclico), lo aceptamos
				if (!contieneCiclos(vertex, next)) {
					mensajeAEnviar = next.toString() + getSeparadorDeVertices()
							+ vertex.getId();

					// mensajesDeVerticesPredecesores += separador
					// + mensajeAEnviar;
					mensajesDeVerticesPredecesoresStringBuilder.append(
							separador).append(mensajeAEnviar);

					mensajesAEnviar.add(mensajeAEnviar);
					separador = ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA;
				} else {
					// Es un ciclo, no se realiza ninguna acción
					// LOG.debug("Vertex ID: " + vertex.getId()
					// + "- descartando ciclo:" + next.toString());
				}
			}

			mensajesDeVerticesPredecesores = mensajesDeVerticesPredecesoresStringBuilder
					.toString();

			mensajesDeVerticesPredecesores = ParserUtils
					.removerPrimerSeparador(mensajesDeVerticesPredecesores);

			mensajesDeVerticesPredecesores = ParserUtils
					.removerUltimoSeparador(mensajesDeVerticesPredecesores);

			// Configuramos nuevamente los mensajes de vertices predecesores,
			// agregando los nuevos mensajes (exceptuando los ciclos)
			vertex.getValue().setMensajesVerticesPredecesores(
					mensajesDeVerticesPredecesores);
		}

		return mensajesAEnviar;
	}

	private Boolean contieneCiclos(
			Vertex<Text, TextAndDoubleComplexWritable, DoubleWritable> vertex,
			Text mensajeDeUnPredecesor) {
		// TODO: Cambiar, debe hacerse un split y separar los vertices antes de
		// preguntar si el mismo está contenido o no
		return mensajeDeUnPredecesor.toString().contains(
				vertex.getId().toString());

	}
}
