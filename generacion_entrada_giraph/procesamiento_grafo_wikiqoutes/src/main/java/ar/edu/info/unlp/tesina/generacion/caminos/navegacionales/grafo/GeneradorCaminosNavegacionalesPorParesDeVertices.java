package ar.edu.info.unlp.tesina.generacion.caminos.navegacionales.grafo;

import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote.DESTINATION_ID;
import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote.MAX_SUPERSTEPS;
import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote.PARES_INICIO_DESTINO;
import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote.SEPARADOR_VERTICES;
import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote.SOURCE_ID;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.giraph.GiraphRunner;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.job.GiraphJob;
import org.apache.giraph.utils.FileUtils;
import org.apache.giraph.utils.InternalVertexRunner;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import ar.edu.info.unlp.tesina.lectura.archivo.ParserUtils;
import ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesWikiquote;
import ar.edu.info.unlp.tesina.vertice.estructuras.IdTextWithComplexValueInputFormat;
import ar.edu.info.unlp.tesina.vertice.estructuras.IdTextWithComplexValueOutputFormat;

/**
 * Permite generar los caminos navegacionales para un grafo dado, indicando a
 * traves de parametros la profundidad maxima de los caminos navegacionales y el
 * vertice que se debe tomar como origen. Se usan los primeros dos parametros,
 * el resto son redireccionados para que Giraph haga uso de ellos
 * 
 * <ul>
 * <li>Profundidad maxima de camino navegacional: args[0]</li>
 * <li>Vertice de origen: args[1]</li>
 * <li>Vertice de destino: args[2]</li>
 * </ul>
 * 
 * @author jlarroque
 *
 */
public class GeneradorCaminosNavegacionalesPorParesDeVertices implements Tool {

	private static final String[] EMPTY_STRING_ARRAY = new String[0];

	public static void main(String[] args) throws Exception {
		new GeneradorCaminosNavegacionalesPorParesDeVertices().run(args);
	}

	/**
	 * Helper method to remove an old output directory if it exists, and set the
	 * output path for any VertexOutputFormat that uses FileOutputFormat.
	 *
	 * @param job
	 *            Job to set the output path for
	 * @param outputPath
	 *            Path to output
	 * @throws IOException
	 */
	public static void removeAndSetOutput(GiraphJob job, Path outputPath)
			throws IOException {
		FileUtils.deletePath(job.getConfiguration(), outputPath);
		FileOutputFormat.setOutputPath(job.getInternalJob(), outputPath);
	}

	/**
	 * Prepare a GiraphJob
	 *
	 * @param name
	 *            identifying name for job
	 * @param conf
	 *            GiraphConfiguration describing which classes to use
	 * @param outputPath
	 *            Where to right output to
	 * @return GiraphJob configured for testing
	 * @throws IOException
	 *             if anything goes wrong
	 */
	protected static GiraphJob prepareJob(String name, Path outputPath,
			GiraphConfiguration configuracion) throws IOException {
		GiraphJob job = new GiraphJob(configuracion, name);
		if (outputPath != null) {
			removeAndSetOutput(job, outputPath);
		}
		return job;
	}

	public void setConf(Configuration conf) {
	}

	public Configuration getConf() {
		return null;
	}

	public int run(String[] args) throws Exception {

		// Cambiar flag para modo TEST
		Boolean modoTest = Boolean.FALSE;

		String parametroLongitudCaminosNavegacionales = args[0];
		String parametroConjuntoVertices = args[1];
		String rutaHdfs = args[2];
		String parametroSeparadorUsuario = args[3];

		GiraphConfiguration giraphConfiguration = new GiraphConfiguration();

		GiraphRunner giraphRunner = new GiraphRunner();

		System.out.println("----------------------------------------");
		System.out.println("Iniciando BFS:" + parametroConjuntoVertices);
		System.out.println("----------------------------------------");

		args = argumentosGiraph(args);

		// Ruta HDFS donde se guardan los resultados
		args[8] = args[8] + "/" + rutaHdfs;

		SEPARADOR_VERTICES.set(giraphConfiguration, parametroSeparadorUsuario);

		SOURCE_ID.set(giraphConfiguration, ParserUtils.obtenerVerticesInicio(
				parametroConjuntoVertices, parametroSeparadorUsuario));

		DESTINATION_ID.set(giraphConfiguration, ParserUtils
				.obtenerVerticesDestino(parametroConjuntoVertices,
						parametroSeparadorUsuario));

		PARES_INICIO_DESTINO
				.set(giraphConfiguration, parametroConjuntoVertices);

		MAX_SUPERSTEPS.set(giraphConfiguration,
				Integer.valueOf(parametroLongitudCaminosNavegacionales));

		// Borro si existe el directorio, de otra forma explota
		FileUtils.deletePath(giraphConfiguration, args[8]);

		giraphRunner.setConf(giraphConfiguration);

		int result = 0;

		if (modoTest) {// Ejecucion desde dentro del eclipse
			giraphConfiguration
					.setComputationClass(BusquedaDeCaminosNavegacionalesWikiquote.class);
			giraphConfiguration
					.setVertexInputFormatClass(IdTextWithComplexValueInputFormat.class);
			giraphConfiguration
					.setVertexOutputFormatClass(IdTextWithComplexValueOutputFormat.class);

			// Levantamos el archivo de entrada en forma manual, para pasarselo
			// al InternalVertexRunner
			List<String> archivoEntradaGiraph = ar.edu.info.unlp.tesina.lectura.archivo.FileUtils
					.leerArchivoDesdeDisco(args[4]);
			InternalVertexRunner.run(giraphConfiguration,
					archivoEntradaGiraph.toArray((new String[0])));

		} else {// Modo comun, ejecucion desde linea de comandos

			result = giraphRunner.run(args);
			System.out.println("----------------------------------------");
			System.out.println("El resultado BFS con  Origen: "
					+ parametroConjuntoVertices + " es:" + result);
			System.out.println("----------------------------------------");
		}
		return result;
	}

	/**
	 * Eliminamos los parametros de configuracion propios de nuestra aplicación
	 * y ajenos a los propios de Giraph
	 * 
	 * @param args_originales
	 * @return
	 */
	public String[] argumentosGiraph(String[] args_originales) {

		List<String> list = new ArrayList<String>();
		String[] array = list.toArray(new String[list.size()]);
		Collections.addAll(list, args_originales);
		list.remove(0);
		list.remove(0);
		list.remove(0);
		list.remove(0);
		array = list.toArray(EMPTY_STRING_ARRAY);
		return array;
	}

}
