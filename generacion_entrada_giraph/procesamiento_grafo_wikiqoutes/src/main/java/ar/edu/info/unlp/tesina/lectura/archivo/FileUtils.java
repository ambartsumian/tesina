package ar.edu.info.unlp.tesina.lectura.archivo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

/**
 * Clase para hacer operaciones basicas con archivos
 * 
 * @author jlarroque
 *
 */
public class FileUtils {
	private static final Logger LOG = Logger.getLogger(FileUtils.class);

	/**
	 * @deprecated Usar {@link #leerArchivoDesdeDisco(String)}
	 * @param hdfs
	 * @param direccionDelArchivo
	 * @return
	 */
	@Deprecated
	public static List<String> leerArchivoDesdeHDFS(FileSystem hdfs,
			Path direccionDelArchivo) {
		List<String> archivoLeido = new ArrayList<String>();
		LOG.info("El archivo " + direccionDelArchivo
				+ " existe, se lo abre para su lectura");
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					hdfs.open(direccionDelArchivo)));
			String lineaLeidaDelArchivo = br.readLine();
			while (lineaLeidaDelArchivo != null) {
				archivoLeido.add(lineaLeidaDelArchivo);
				lineaLeidaDelArchivo = br.readLine();
			}
			LOG.info("Fin de lectura del archivo " + direccionDelArchivo);
		} catch (Exception e) {
			LOG.error("El archivo " + direccionDelArchivo
					+ " no existe en el HDFS " + e.toString());
		}
		return archivoLeido;
	}

	public static List<String> leerArchivoDesdeDisco(String direccionDelArchivo) {
		List<String> archivoLeido = new ArrayList<String>();
		LOG.info("El archivo " + direccionDelArchivo
				+ " existe, se lo abre para su lectura");
		try {
			archivoLeido = org.apache.commons.io.FileUtils.readLines(new File(
					direccionDelArchivo));
		} catch (IOException e) {

			LOG.error("El archivo " + direccionDelArchivo
					+ " no existe en el FS " + e.toString());
		}

		LOG.info("Fin de lectura del archivo " + direccionDelArchivo);

		return archivoLeido;
	}
}
