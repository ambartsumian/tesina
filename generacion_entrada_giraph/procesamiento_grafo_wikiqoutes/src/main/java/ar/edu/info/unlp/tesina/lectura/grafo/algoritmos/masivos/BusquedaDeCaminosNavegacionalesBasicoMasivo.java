package ar.edu.info.unlp.tesina.lectura.grafo.algoritmos.masivos;

import static ar.edu.info.unlp.tesina.lectura.grafo.BusquedaDeCaminosNavegacionalesBasico.SOURCE_ID;

import java.io.IOException;

import org.apache.giraph.GiraphRunner;
import org.apache.giraph.conf.GiraphConfiguration;
import org.apache.giraph.job.GiraphJob;
import org.apache.giraph.utils.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

@Deprecated
public class BusquedaDeCaminosNavegacionalesBasicoMasivo {
	public static void main(String[] args) throws Exception {

		if (args.length < 4) {
			System.err.println("Pocos parametros");
			System.exit(1);
		}

		GiraphConfiguration giraphConfiguration = new GiraphConfiguration();

		Long id_actual = (long) 1;
		GiraphRunner giraphRunner = new GiraphRunner();

		while (id_actual < 4) {
			System.out.println("----------------------------------------");
			System.out.println("Iniciando BFS con ID:" + id_actual);
			System.out.println("----------------------------------------");

			args[8] = args[8] + id_actual;

			SOURCE_ID.set(giraphConfiguration, id_actual);

			// Borro si existe el directorio, de otra forma explota
			FileUtils.deletePath(giraphConfiguration, args[8]);

			giraphRunner.setConf(giraphConfiguration);
			int result = giraphRunner.run(args);
			// boolean result = ToolRunner.run(job.getInternalJob(),
			// giraphRunner);
			System.out.println("----------------------------------------");
			System.out.println("El resultado del BFS con ID:" + id_actual
					+ " result es:" + result);
			System.out.println("----------------------------------------");
			id_actual++;
		}

		System.exit(id_actual == 4 ? 1 : 0);
	}

	/**
	 * Helper method to remove an old output directory if it exists, and set the
	 * output path for any VertexOutputFormat that uses FileOutputFormat.
	 *
	 * @param job
	 *            Job to set the output path for
	 * @param outputPath
	 *            Path to output
	 * @throws IOException
	 */
	public static void removeAndSetOutput(GiraphJob job, Path outputPath)
			throws IOException {
		FileUtils.deletePath(job.getConfiguration(), outputPath);
		FileOutputFormat.setOutputPath(job.getInternalJob(), outputPath);
	}

	/**
	 * Prepare a GiraphJob
	 *
	 * @param name
	 *            identifying name for job
	 * @param conf
	 *            GiraphConfiguration describing which classes to use
	 * @param outputPath
	 *            Where to right output to
	 * @return GiraphJob configured for testing
	 * @throws IOException
	 *             if anything goes wrong
	 */
	protected static GiraphJob prepareJob(String name, Path outputPath,
			GiraphConfiguration configuracion) throws IOException {
		GiraphJob job = new GiraphJob(configuracion, name);
		setupConfiguration(job);
		if (outputPath != null) {
			removeAndSetOutput(job, outputPath);
		}
		return job;
	}

	private static void setupConfiguration(GiraphJob job) {

	}
}
