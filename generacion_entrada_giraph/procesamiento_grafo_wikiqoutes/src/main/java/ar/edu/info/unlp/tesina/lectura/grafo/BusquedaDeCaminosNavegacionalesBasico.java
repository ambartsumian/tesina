package ar.edu.info.unlp.tesina.lectura.grafo;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

import org.apache.giraph.conf.LongConfOption;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.examples.Algorithm;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.log4j.Logger;

/**
 * Busca todos los caminos navegacionales. Adaptacion de
 * https://github.com/MarcoLotz
 * /GiraphBFSSO/blob/master/src/uk/co/qmul/giraph/structurebfs
 * /SimpleBFSStructureComputation.java de @MarcoLotz
 */
@Algorithm(name = "Busqueda de Caminos Navegacionales", description = "Busca todos los caminos navegacionales de un grafo")
public class BusquedaDeCaminosNavegacionalesBasico
		extends
		BasicComputation<LongWritable, DoubleWritable, FloatWritable, DoubleWritable> {
	/** Id origen de la busqueda de caminos navegacionales */
	public static final LongConfOption SOURCE_ID = new LongConfOption(
			"BusquedaDeCaminosNavegacionales.sourceId", 1,
			"El vertice de origen, de la busqueda de todos los caminos navegacionales");

	/** Class logger */
	private static final Logger LOG = Logger
			.getLogger(BusquedaDeCaminosNavegacionalesBasico.class);

	/**
	 * Define a maximum number of supersteps
	 */
	public final int MAX_SUPERSTEPS = 9999;

	/**
	 * Is this vertex the source id?
	 *
	 * @param vertex
	 *            Vertex
	 * @return True if the source id
	 */
	private boolean isStart(Vertex<LongWritable, ?, ?> vertex) {
		return vertex.getId().get() == SOURCE_ID.get(getConf());
	}

	/**
	 * Send messages to all the connected vertices. The content of the messages
	 * is not important, since just the event of receiving a message removes the
	 * vertex from the inactive status.
	 * 
	 * @param vertex
	 */
	public void BFSMessages(
			Vertex<LongWritable, DoubleWritable, FloatWritable> vertex) {
		for (Edge<LongWritable, FloatWritable> edge : vertex.getEdges()) {
			sendMessage(edge.getTargetVertexId(), new DoubleWritable(1d));
		}
	}

	@Override
	public void compute(
			Vertex<LongWritable, DoubleWritable, FloatWritable> vertex,
			Iterable<DoubleWritable> messages) throws IOException {
		// Forces convergence in maximum superstep
		if (!(getSuperstep() == MAX_SUPERSTEPS)) {
			// Only start vertex should work in the first superstep
			// All the other should vote to halt and wait for
			// messages.
			if (getSuperstep() == 0) {
				if (isStart(vertex)) {
					vertex.setValue(new DoubleWritable(getSuperstep()));
					BFSMessages(vertex);
					LOG.info("[Start Vertex] Vertex ID: " + vertex.getId());

				} else { // Initialise with infinite depth other vertex
					vertex.setValue(new DoubleWritable(Integer.MAX_VALUE));
				}
			}

			// if it is not the first Superstep (Superstep 0) :
			// Check vertex ID

			else {
				// It is the first time that this vertex is being computed
				if (vertex.getValue().get() == Integer.MAX_VALUE) {
					// The depth has the same value that the superstep
					vertex.setValue(new DoubleWritable(getSuperstep()));
					// Continue on the structure
					BFSMessages(vertex);
				}
				// Else this vertex was already analysed in a previous
				// iteration.
			}
			vertex.voteToHalt();
		}
	}
}
