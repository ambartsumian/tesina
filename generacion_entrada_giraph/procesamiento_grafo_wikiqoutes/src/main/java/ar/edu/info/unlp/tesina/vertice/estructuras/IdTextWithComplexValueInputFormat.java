package ar.edu.info.unlp.tesina.vertice.estructuras;

import java.io.IOException;

import org.apache.giraph.edge.Edge;
import org.apache.giraph.edge.EdgeFactory;
import org.apache.giraph.io.formats.AdjacencyListTextVertexInputFormat;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.log4j.Logger;

/**
 * @author jlarroque
 *
 */
public class IdTextWithComplexValueInputFormat
		extends
		AdjacencyListTextVertexInputFormat<Text, TextAndDoubleComplexWritable, DoubleWritable> {

	private static final Logger LOG = Logger
			.getLogger(IdTextWithComplexValueInputFormat.class);

	/**
	 * Utility for doing any cleaning of each line before it is tokenized.
	 */
	public interface LineSanitizer {
		/**
		 * Clean string s before attempting to tokenize it.
		 *
		 * @param s
		 *            String to be cleaned.
		 * @return Sanitized string.
		 */
		String sanitize(String s);
	}

	@Override
	public TextComplexValueDoubleAdjacencyListVertexReader createVertexReader(
			InputSplit inputSplit, TaskAttemptContext context) {
		return new TextComplexValueDoubleAdjacencyListVertexReader(null);
	}

	protected class TextComplexValueDoubleAdjacencyListVertexReader extends
			AdjacencyListTextVertexReader {

		/** Cached delimiter used for split */
		private String splitValue = null;

		/** Sanitizer from constructor. */
		private final LineSanitizer sanitizer;

		public TextComplexValueDoubleAdjacencyListVertexReader() {
			this(null);
		}

		public TextComplexValueDoubleAdjacencyListVertexReader(
				LineSanitizer sanitizer) {
			this.sanitizer = sanitizer;
		}

		@Override
		public void initialize(InputSplit inputSplit, TaskAttemptContext context)
				throws IOException, InterruptedException {
			super.initialize(inputSplit, context);
			splitValue = getConf().get(LINE_TOKENIZE_VALUE,
					LINE_TOKENIZE_VALUE_DEFAULT);
		}

		@Override
		protected String[] preprocessLine(Text line) throws IOException {
			String sanitizedLine;
			if (sanitizer != null) {
				sanitizedLine = sanitizer.sanitize(line.toString());
			} else {
				sanitizedLine = line.toString();
			}
			String[] values = sanitizedLine.split(splitValue);
			if ((values.length < 2) || (values.length % 2 != 0)) {
				throw new IllegalArgumentException(
						"Line did not split correctly: " + line);
			}
			return values;
		}

		@Override
		public Text decodeId(String s) {

			// LOG.debug("Vertex ID - Valor decodificado: " + s
			// + " - decodificacion vertice");

			return new Text(s);
		}

		@Override
		public TextAndDoubleComplexWritable decodeValue(String s) {
			TextAndDoubleComplexWritable valorComplejo = new TextAndDoubleComplexWritable();
			valorComplejo.setVertexData(Double.valueOf(s));
			valorComplejo.setMensajesVerticesPredecesores("");
			valorComplejo.setVerticeDestino(Boolean.FALSE);

			// LOG.debug("Vertex value - Valor decodificado: " + s
			// + " - decodificacion vertice");

			return valorComplejo;
		}

		@Override
		public Edge<Text, DoubleWritable> decodeEdge(String s1, String s2) {

			// LOG.debug("Input vertex, valor arista: " + s1 + "-"
			// + Double.valueOf(s2));

			// LOG.debug("Arista - Valor decodificado: " + s1 + "-"
			// + Double.valueOf(s2) + " - decodificacion arista");

			return EdgeFactory.create(new Text(s1),
					new DoubleWritable(Double.valueOf(s2)));
		}
	}

}
