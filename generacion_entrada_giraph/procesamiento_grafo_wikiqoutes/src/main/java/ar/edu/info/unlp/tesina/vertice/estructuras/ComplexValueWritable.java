package ar.edu.info.unlp.tesina.vertice.estructuras;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * Clase gemela de {@link TextAndDoubleComplexWritable}, solo que usando el
 * valor double de una manera distinta (sin leerlo desde el archivo de entrada)
 * 
 * @author jlarroque
 */
public class ComplexValueWritable implements Writable {

	public static final String LINE_TOKENIZE_VALUE_DEFAULT = "\t";

	private String idsVerticesAnteriores;

	private double vertexData;

	public ComplexValueWritable() {
		super();
		this.idsVerticesAnteriores = "";
		/*
		 * Este valor tendran los vertices que no esten listados como tal en el
		 * archivo de entrada
		 */
		this.vertexData = new Double(Integer.MAX_VALUE);
	}

	public ComplexValueWritable(double vertexData) {
		super();
		this.vertexData = vertexData;
	}

	public ComplexValueWritable(String ids_vertices_anteriores,
			double vertexData) {
		super();
		this.idsVerticesAnteriores = ids_vertices_anteriores;
		this.vertexData = vertexData;
	}

	public void write(DataOutput out) throws IOException {
		out.writeUTF(this.idsVerticesAnteriores != "" ? "hola"
				: this.idsVerticesAnteriores);
	}

	public void readFields(DataInput in) throws IOException {
		this.idsVerticesAnteriores = in.readUTF();
	}

	public String getIds_vertices_anteriores() {
		return idsVerticesAnteriores;
	}

	public void setIds_vertices_anteriores(String ids_vertices_anteriores) {
		this.idsVerticesAnteriores = ids_vertices_anteriores;
	}

	public double getVertexData() {
		return vertexData;
	}

	public void setVertexData(double vertexData) {
		this.vertexData = vertexData;
	}

	@Override
	public String toString() {
		return String.valueOf(vertexData)
				+ idsVerticesAnteriores.split(LINE_TOKENIZE_VALUE_DEFAULT)
						.toString();
	}
}
