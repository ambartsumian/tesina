package ar.edu.info.unlp.tesina.lectura.archivo;

import ar.edu.info.unlp.constants.ConstantesDeProcesamientoDeTexto;

/**
 * Clase interna que nos permite parsear de distinta forma los strings
 * 
 * @author jlarroque
 *
 */
public class ParserUtils {

	public static String obtenerVerticesInicio(String PARES_INICIO_DESTINO,
			String SEPARADOR_VERTICES) {
		String resultado = "";
		String[] paresDeVertices = obtenerParesDeVertices(PARES_INICIO_DESTINO);
		String[] verticeInicioYVerticeDestino;
		String separador = "";
		for (String str : paresDeVertices) {
			verticeInicioYVerticeDestino = obtenerSeparacionEntreInicioYDestino(
					str, SEPARADOR_VERTICES);
			resultado += separador + verticeInicioYVerticeDestino[0];
			separador = SEPARADOR_VERTICES;
		}

		return resultado;

	}

	public static String obtenerVerticesDestino(String PARES_INICIO_DESTINO,
			String SEPARADOR_VERTICES) {
		String resultado = "";
		String[] paresDeVertices = obtenerParesDeVertices(PARES_INICIO_DESTINO);
		String[] verticeInicioYVerticeDestino;
		String separador = "";
		for (String str : paresDeVertices) {
			verticeInicioYVerticeDestino = obtenerSeparacionEntreInicioYDestino(
					str, SEPARADOR_VERTICES);
			resultado += separador + verticeInicioYVerticeDestino[1];
			separador = SEPARADOR_VERTICES;
		}

		return resultado;
	}

	public static String[] obtenerSeparacionEntreInicioYDestino(String str,
			String SEPARADOR_VERTICES) {
		return str.split(SEPARADOR_VERTICES);
	}

	public static boolean esParInicioDestinoValido(String PARES_INICIO_DESTINO,
			String verticeInicio, String verticeDestino,
			String SEPARADOR_VERTICES) {
		String[] paresDeVertices = obtenerParesDeVertices(PARES_INICIO_DESTINO);
		String[] verticeInicioYVerticeDestino;
		for (String str : paresDeVertices) {
			verticeInicioYVerticeDestino = obtenerSeparacionEntreInicioYDestino(
					str, SEPARADOR_VERTICES);
			if (verticeInicioYVerticeDestino[0].equals(verticeInicio)
					&& verticeInicioYVerticeDestino[1].equals(verticeDestino)) {
				return true;
			}
		}
		return false;
	}

	private static String[] obtenerParesDeVertices(String PARES_INICIO_DESTINO) {
		return PARES_INICIO_DESTINO
				.split(ConstantesDeProcesamientoDeTexto.SEPARADOR_PARES_VERTICES);
	}

	/**
	 * En caso de que tenga un \t al principio del string, se le remueve
	 *
	 * @param idsDeVerticesPredecesores
	 * @return
	 */
	public static String removerPrimerSeparador(String idsDeVerticesPredecesores) {

		if (idsDeVerticesPredecesores.length() > 0
				&& idsDeVerticesPredecesores
						.substring(0, 1)
						.equals(ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA)) {
			// Si solamente tenemos un separador de linea, devolvemos un string
			// vacio
			if (idsDeVerticesPredecesores
					.equals(ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA)) {
				return "";
			} else
			// Devolvemos el resto del string, despues de separarlo del
			// separador de linea (valga la redundancia)
			{
				return idsDeVerticesPredecesores.substring(1,
						idsDeVerticesPredecesores.length() - 1);
			}
		} else {
			return idsDeVerticesPredecesores;
		}
	}

	/**
	 * En caso de que tenga un \t al final del string, se le remueve
	 *
	 * @param idsDeVerticesPredecesores
	 * @return
	 */
	public static String removerUltimoSeparador(String idsDeVerticesPredecesores) {

		if (idsDeVerticesPredecesores.length() > 0
				&& idsDeVerticesPredecesores
						.substring(idsDeVerticesPredecesores.length() - 1)
						.equals(ConstantesDeProcesamientoDeTexto.SEPARADOR_VALORES_EN_LINEA)) {
			return idsDeVerticesPredecesores.substring(0,
					idsDeVerticesPredecesores.length() - 1);
		} else {
			return idsDeVerticesPredecesores;
		}
	}

}
