package ar.edu.info.unlp.tesina.lectura.grafo.algoritmos.masivos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.Tool;
import org.apache.log4j.Logger;

import ar.edu.info.unlp.constants.ConstantesDeProcesamientoDeTexto;
import ar.edu.info.unlp.tesina.generacion.caminos.navegacionales.grafo.GeneradorCaminosNavegacionalesPorParesDeVertices;
import ar.edu.info.unlp.tesina.lectura.archivo.ParserUtils;

/**
 * Se calculan los caminos navegacionales desde distintos pares de vertices de
 * origen - destino a la vez.
 *
 * Parametros:<br/>
 * - Direccion del archivo con los conjuntos de pares de vertices<br/>
 * - Cantidad de superpasos<br/>
 * - Separador de vertices a usar</br>
 * 
 * @author jlarroque
 *
 */
public class BusquedaDeCaminosNavegacionalesWikiquotesMasivo implements Tool {

	private static final String[] EMPTY_STRING_ARRAY = new String[0];

	private static final Logger LOG = Logger
			.getLogger(BusquedaDeCaminosNavegacionalesWikiquotesMasivo.class);

	public static void main(String[] args) throws Exception {
		new BusquedaDeCaminosNavegacionalesWikiquotesMasivo().run(args);
	}

	public void setConf(Configuration conf) {

	}

	public Configuration getConf() {
		return null;
	}

	public int run(String[] args) throws Exception {
		GeneradorCaminosNavegacionalesPorParesDeVertices generadorCaminosNavegacionales = new GeneradorCaminosNavegacionalesPorParesDeVertices();

		String[] parametrosGeneracionCaminosNavegacionalesPorVertice = null;

		// Ids de vertices
		List<String> conjuntosDeVerticesLeidosDesdeElDisco = this
				.verticesAProcesarDesdeDisco(args[0]);

		Integer cantidadParesDeVerticesSimultaneos = Integer.parseInt(args[3]);

		args = removerNombreArchivoVerticesDeParametrosDeConfiguracion(args);

		args = removerCantidadDeParesDeVerticesDeParametrosDeConfiguracion(args);

		String conjuntoDeParesDeVerticesActuales = null;

		String separadorEntreVerticeInicioYDestino = args[1];

		ListIterator<String> iteradorListaVertices = conjuntosDeVerticesLeidosDesdeElDisco
				.listIterator();

		// Recorremos cada vertice de la lista, generando los caminos
		// navegacionales del mismo

		Integer iteracionSobreParesDeVerticeActual = 0;

		while (iteradorListaVertices.hasNext()) {

			// Obtenemos el vertice origen del BFS
			conjuntoDeParesDeVerticesActuales = obtenerParesDeVertices(
					iteradorListaVertices, cantidadParesDeVerticesSimultaneos,
					separadorEntreVerticeInicioYDestino);

			// Agregamos el conjunto de vertices en los parametros originales
			// (en la
			// segunda posicion) y la ruta para dejar los resultados en HDFS en
			// la tercera
			iteracionSobreParesDeVerticeActual++;
			parametrosGeneracionCaminosNavegacionalesPorVertice = agregarParametrosDeConfiguracion(
					args, conjuntoDeParesDeVerticesActuales,
					iteracionSobreParesDeVerticeActual);

			// Ejecutamos el BFS
			int result = generadorCaminosNavegacionales
					.run(parametrosGeneracionCaminosNavegacionalesPorVertice);

		}

		System.out
				.println("Se terminaron de procesar todos los pares de vertices, consulte HDFS para conocer los caminos navegacionales encontrados");
		LOG.info("Se terminaron de procesar todos los pares de vertices, consulte HDFS para conocer los caminos navegacionales encontrados");
		return !iteradorListaVertices.hasNext() ? 1 : 0;
	}

	/**
	 * @param iteradorListaVertices
	 * @param cantidad_maxima
	 * @param separadorEntreVerticeInicioYDestino
	 * @return
	 */
	private String obtenerParesDeVertices(
			ListIterator<String> iteradorListaVertices,
			Integer cantidad_maxima, String separadorEntreVerticeInicioYDestino) {
		StringBuilder strBuilder = new StringBuilder();

		Integer cantidad = 1;

		String parDeVerticesActual = iteradorListaVertices.next();

		String verticeInicioActual = obtenerVerticeInicio(
				separadorEntreVerticeInicioYDestino, parDeVerticesActual);
		strBuilder.append(parDeVerticesActual);

		while (iteradorListaVertices.hasNext() && cantidad < cantidad_maxima) {
			parDeVerticesActual = siguienteParDeVertice(iteradorListaVertices);
			if (verticeInicioActual.equals(obtenerVerticeInicio(
					separadorEntreVerticeInicioYDestino,
					descartarSeparadorDeVertices(parDeVerticesActual)))) {
				strBuilder.append(parDeVerticesActual);
				cantidad++;
			} else {
				iteradorListaVertices.previous();
				cantidad = cantidad_maxima;
			}

		}

		return strBuilder.toString();
	}

	/**
	 * @param parDeVerticesActual
	 * @return
	 */
	private String descartarSeparadorDeVertices(String parDeVerticesActual) {
		return parDeVerticesActual.replace(
				ConstantesDeProcesamientoDeTexto.SEPARADOR_PARES_VERTICES, "");
	}

	/**
	 * @param separadorEntreVerticeInicioYDestino
	 * @param parDeVerticesActual
	 * @return
	 */
	private String obtenerVerticeInicio(
			String separadorEntreVerticeInicioYDestino,
			String parDeVerticesActual) {
		return ParserUtils.obtenerSeparacionEntreInicioYDestino(
				parDeVerticesActual, separadorEntreVerticeInicioYDestino)[0];
	}

	/**
	 * Nos devuelve el siguiente par de vertices, en caso de que haya uno
	 * disponible
	 * 
	 * @param iteradorListaVertices
	 * @return
	 */
	private String siguienteParDeVertice(
			ListIterator<String> iteradorListaVertices) {
		return iteradorListaVertices.hasNext() ? ConstantesDeProcesamientoDeTexto.SEPARADOR_PARES_VERTICES
				+ iteradorListaVertices.next()
				: "";
	}

	private String[] removerNombreArchivoVerticesDeParametrosDeConfiguracion(
			String[] args_originales) {
		List<String> list = new ArrayList<String>();
		String[] array = list.toArray(new String[list.size()]);
		Collections.addAll(list, args_originales);

		list.remove(0);
		array = list.toArray(EMPTY_STRING_ARRAY);
		return array;

	}

	private String[] removerCantidadDeParesDeVerticesDeParametrosDeConfiguracion(
			String[] args_originales) {
		List<String> list = new ArrayList<String>();
		String[] array = list.toArray(new String[list.size()]);
		Collections.addAll(list, args_originales);

		list.remove(2);
		array = list.toArray(EMPTY_STRING_ARRAY);
		return array;

	}

	/**
	 * Permite agregar, en la segunda posicion del arreglo de parametros, el
	 * conjunto de pares de vertice origen-destino para la busqueda de caminos
	 * navegacionales
	 * 
	 * @param args_originales
	 * @param iteracionSobreParesDeVerticeActual
	 * @return
	 */
	private String[] agregarParametrosDeConfiguracion(String[] args_originales,
			String conjDeVertices, Integer iteracionSobreParesDeVerticeActual) {
		List<String> list = new ArrayList<String>();
		String[] array = list.toArray(new String[list.size()]);
		Collections.addAll(list, args_originales);

		// Agregamos como parametro los vertices origen y destino
		list.add(1, conjDeVertices);
		list.add(2, iteracionSobreParesDeVerticeActual.toString());

		array = list.toArray(EMPTY_STRING_ARRAY);
		return array;
	}

	/**
	 * Obtenemos los pares de vertices a procesar, accediendo desde un archivo
	 * de disco
	 * 
	 * @param pathHdfsArchivoVertices
	 * @return
	 */
	private List<String> verticesAProcesarDesdeDisco(
			String pathHdfsArchivoVertices) {
		return ar.edu.info.unlp.tesina.lectura.archivo.FileUtils
				.leerArchivoDesdeDisco(pathHdfsArchivoVertices);
	}
}
