package ar.edu.info.unlp.tesina.lectura.grafo;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

import org.apache.giraph.conf.IntConfOption;
import org.apache.giraph.conf.StrConfOption;
import org.apache.giraph.edge.Edge;
import org.apache.giraph.examples.Algorithm;
import org.apache.giraph.graph.BasicComputation;
import org.apache.giraph.graph.Vertex;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import ar.edu.info.unlp.tesina.vertice.estructuras.TextAndDoubleComplexWritable;

/**
 * Clase gemela de {@link BusquedaDeCaminosNavegacionalesWikiquote}
 */
@Deprecated
@Algorithm(name = "Busqueda de Caminos Navegacionales", description = "Busca todos los caminos navegacionales de un grafo, preparado para grafos de datos de Wikiquotes")
public class BusquedaEntreDosVerticesDeCaminosNavegacionalesWikiquote
		extends
		BasicComputation<Text, TextAndDoubleComplexWritable, NullWritable, Text> {

	/** Class logger */
	private static final Logger LOG = Logger
			.getLogger(BusquedaEntreDosVerticesDeCaminosNavegacionalesWikiquote.class);

	/**
	 * Id origen de la busqueda de caminos navegacionales, indica el primer
	 * vertice a procesar en el superstep 0
	 */
	public static final StrConfOption SOURCE_ID = new StrConfOption(
			"BusquedaDeCaminosNavegacionales.sourceId", "Portada",
			"El vertice de origen, de la busqueda de todos los caminos navegacionales");

	public static final StrConfOption DEST_ID = new StrConfOption(
			"BusquedaDeCaminosNavegacionales.destId", "",
			"El vertice de destino, de la busqueda de todos los caminos navegacionales");

	/**
	 * Define a maximum number of supersteps
	 */
	public static final IntConfOption MAX_SUPERSTEPS = new IntConfOption(
			"BusquedaDeCaminosNavegacionales.supersteps", 5,
			"Cantidad maxima de superpasos");

	/**
	 * Separador de los valores en una misma linea
	 */
	public static final String LINE_TOKENIZE_VALUE_DEFAULT = "\t";

	/**
	 * Is this vertex the source id?
	 *
	 * @param vertex
	 *            Vertex
	 * @return True if the source id
	 */
	private boolean isStart(Vertex<Text, ?, ?> vertex) {
		return vertex.getId().toString()
				.equals(SOURCE_ID.get(getConf()).toString());
	}

	/**
	 * ¿El vertice es el de destino?
	 *
	 * @param vertex
	 *            Vertex
	 * @return True if the source id
	 */
	private boolean isDest(Vertex<Text, ?, ?> vertex) {
		return vertex.getId().toString()
				.equals(DEST_ID.get(getConf()).toString());
	}

	private int maximaCantidadDeSuperpasosPosibles() {
		return MAX_SUPERSTEPS.get(getConf());
	}

	/**
	 * Send messages to all the connected vertices. The content of the messages
	 * is not important, since just the event of receiving a message removes the
	 * vertex from the inactive status.
	 * 
	 * @param vertex
	 */
	public void BFSMessages(
			Vertex<Text, TextAndDoubleComplexWritable, NullWritable> vertex) {
		for (Edge<Text, NullWritable> edge : vertex.getEdges()) {
			sendMessage(edge.getTargetVertexId(), vertex.getId());
		}
	}

	@Override
	public void compute(
			Vertex<Text, TextAndDoubleComplexWritable, NullWritable> vertex,
			Iterable<Text> messages) throws IOException {

		LOG.info("Vertex ID: " + vertex.getId() + " Superstep ID: "
				+ getSuperstep());

		// Se fuerza a que los vertices converjan en el superpaso máximo
		if (getSuperstep() < maximaCantidadDeSuperpasosPosibles()) {
			// Solo el vertice de inicio deberia trabajar en el primer superpaso
			// Todos los demas deberian detenerse y esperar por mensajes

			if (getSuperstep() == 0) {
				if (isStart(vertex)) {
					vertex.getValue().setVertexData(new Double(getSuperstep()));
					BFSMessages(vertex);

					LOG.info("[Vertice inicial] Vertex ID: " + vertex.getId()
							+ " inicializado" + " Superstep ID: "
							+ getSuperstep());

				} else { // Initialise with infinite depth other vertex
					vertex.getValue().setVertexData(
							new Double(Integer.MAX_VALUE));
					LOG.info("Vertex ID: " + vertex.getId() + " Superstep ID: "
							+ getSuperstep() + "inicializado");
				}
			}

			// Si es el superpaso 1 o superior, debemos chequear el Vertex Data
			else {
				// Primera vez que el vertice es procesado (el procesamiento se
				// puede dar en cualquier superpaso, a partir del segundo
				// superpaso ejecutado)
				if (vertex.getValue().getVertexData() == Integer.MAX_VALUE) {
					LOG.info("Vertex ID: " + vertex.getId() + " Superstep ID: "
							+ getSuperstep() + "inicia procesamiento");

					// Seteamos Vertex Data como la profundidad del vertice
					vertex.getValue().setVertexData(new Double(getSuperstep()));

					// Continuar con el resto de la estructura del grafo
					if (isDest(vertex)) {
						// Encontramos el destino, esto quiere decir que se
						// imprimiran caminos navegacionales en {@link
						// IdTextWithComplexValueOutputFormat}
					} else {
						BFSMessages(vertex);
					}
					LOG.info("Vertex ID: " + vertex.getId() + " Superstep ID: "
							+ getSuperstep() + "finaliza procesamiento");
				} else {
					// Este vertice ya fue analizado (por eso no va ELSE)
					LOG.info("Vertex ID: " + vertex.getId() + " Superstep ID: "
							+ getSuperstep()
							+ "fue procesado ya. Se recolectan mensajes");
				}
				configurarVerticesPredecesores(vertex, messages);
			}
			vertex.voteToHalt();
		} else {
			// Si la cantidad de superpasos es la maxima permitida, el vertice
			// se detiene sin importar nada mas
			vertex.voteToHalt();
		}
	}

	/**
	 * Este metodo configura los vertices predecesores. En caso de que la
	 * cantidad maxima de superpasos haya sido alcanzada, permite recolectar
	 * mansajes enviados en el ultimo superpaso valido para procesamiento
	 * 
	 * @param vertex
	 * @param messages
	 */
	private void configurarVerticesPredecesores(
			Vertex<Text, TextAndDoubleComplexWritable, NullWritable> vertex,
			Iterable<Text> messages) {
		// Se configuran los predecesores al vertice en caso de que
		// haya
		if (messages.iterator().hasNext()) {
			// Obtenemos los predecesores hasta el momento (puede haber algunos
			// de un procesamiento previo)
			String idsDeVerticesPredecesores = vertex.getValue()
					.getMensajesVerticesPredecesores();

			// Separamos los vertices predecesores por
			// LINE_TOKENIZE_VALUE_DEFAULT
			for (Text message : messages) {
				idsDeVerticesPredecesores += message.toString()
						+ LINE_TOKENIZE_VALUE_DEFAULT;
			}

			idsDeVerticesPredecesores = removerUltimoSeparador(idsDeVerticesPredecesores);

			vertex.getValue().setMensajesVerticesPredecesores(
					idsDeVerticesPredecesores);
		}
	}

	private String removerUltimoSeparador(String idsDeVerticesPredecesores) {
		return idsDeVerticesPredecesores.substring(0,
				idsDeVerticesPredecesores.length() - 1);
	}
}
