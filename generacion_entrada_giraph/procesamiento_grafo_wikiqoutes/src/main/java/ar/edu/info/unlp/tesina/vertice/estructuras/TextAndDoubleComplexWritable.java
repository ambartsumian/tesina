package ar.edu.info.unlp.tesina.vertice.estructuras;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * Value de un vertice. TODO: CAMBIAR NOMBRE
 * 
 * @author jlarroque
 *
 */
public class TextAndDoubleComplexWritable implements Writable {

	public static final String LINE_TOKENIZE_VALUE_DEFAULT = "\t";

	private String mensajesVerticesPredecesores;

	private double vertexData;

	private Boolean verticeDestino = Boolean.FALSE;

	private String idVerticeInicio = "";

	public TextAndDoubleComplexWritable() {
		super();
		this.mensajesVerticesPredecesores = "";
		/*
		 * Este valor tendran los vertices que no esten listados como tal en el
		 * archivo de entrada
		 */
		this.vertexData = new Double(Integer.MAX_VALUE);
	}

	public TextAndDoubleComplexWritable(double vertexData) {
		super();
		this.vertexData = vertexData;
	}

	public TextAndDoubleComplexWritable(String ids_vertices_anteriores,
			double vertexData) {
		super();
		this.mensajesVerticesPredecesores = ids_vertices_anteriores;
		this.vertexData = vertexData;
	}

	public void write(DataOutput out) throws IOException {
		out.writeDouble(this.vertexData);
		out.writeUTF(this.mensajesVerticesPredecesores != "" ? "hola"
				: this.mensajesVerticesPredecesores);
	}

	public void readFields(DataInput in) throws IOException {
		this.vertexData = in.readDouble();
		this.mensajesVerticesPredecesores = in.readUTF();
	}

	public String getMensajesVerticesPredecesores() {
		return mensajesVerticesPredecesores;
	}

	public void setMensajesVerticesPredecesores(
			String mensajesVerticesPredecesores) {
		this.mensajesVerticesPredecesores = mensajesVerticesPredecesores;
	}

	public double getVertexData() {
		return vertexData;
	}

	public void setVertexData(double vertexData) {
		this.vertexData = vertexData;
	}

	@Override
	public String toString() {
		return String.valueOf(vertexData)
				+ mensajesVerticesPredecesores.split(
						LINE_TOKENIZE_VALUE_DEFAULT).toString();
	}

	public Boolean esVerticeDestino() {
		return verticeDestino;
	}

	public void setVerticeDestino(Boolean verticeDestino) {
		this.verticeDestino = verticeDestino;
	}

	public String getIdVerticeInicio() {
		return idVerticeInicio;
	}

	public void setIdVerticeInicio(String idVerticeInicio) {
		this.idVerticeInicio = idVerticeInicio;
	}
}
