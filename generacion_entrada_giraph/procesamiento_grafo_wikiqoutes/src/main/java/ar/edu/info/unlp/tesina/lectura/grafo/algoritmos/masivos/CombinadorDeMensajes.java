package ar.edu.info.unlp.tesina.lectura.grafo.algoritmos.masivos;

import org.apache.giraph.combiner.MessageCombiner;
import org.apache.hadoop.io.Text;
import org.apache.log4j.Logger;

import ar.edu.info.unlp.constants.ConstantesDeProcesamientoDeTexto;

/**
 * Combinador de mensajes, para reducir la cantidad que le llegan a cada vertice
 * 
 * @author jlarroque
 *
 */
public class CombinadorDeMensajes extends MessageCombiner<Text, Text> {

	private static final Logger LOG = Logger
			.getLogger(CombinadorDeMensajes.class);

	@Override
	public void combine(Text vertexIndex, Text originalMessage,
			Text messageToCombine) {
		// LOG.debug("Vertex ID: " + vertexIndex.toString());
		// LOG.debug("Vertex ID: " + vertexIndex.toString()
		// + "- Mensaje original: " + originalMessage.toString());
		// LOG.debug("Vertex ID: " + vertexIndex.toString()
		// + "- Mensaje a combinar: " + messageToCombine.toString());
		// LOG.debug("Vertex ID: " + messageToCombine.toString());
		originalMessage
				.set(new StringBuilder()
						.append((originalMessage.toString().isEmpty() ? ""
								: originalMessage.toString()
										+ ConstantesDeProcesamientoDeTexto.SEPARADOR_PARES_VERTICES))
						.append(messageToCombine.toString()).toString());
		// LOG.debug("Vertex ID: " + originalMessage.toString()
		// + "- Mensaje combinado: " + originalMessage.toString());
	}

	@Override
	public Text createInitialMessage() {

		return new Text("");
	}

}
